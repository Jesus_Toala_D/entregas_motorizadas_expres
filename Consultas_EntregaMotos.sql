---Primera Consulta-Historico de entregas motorizadas
select
personal_motorizado.cod_motorizado as Codigo_Motorizado,
personal_motorizado.nombres_motorizado as Nombres,
personal_motorizado.apellidos_motorizado as Apellidos,
extract (year from entrega.fecha_entrega) as Fecha,
count (personal_motorizado.cod_motorizado) as Numero_Entregas
from entrega
inner join personal_motorizado on entrega.cod_motorizado=personal_motorizado.cod_motorizado
group by extract (year from entrega.fecha_entrega), personal_motorizado.cod_motorizado,
personal_motorizado.nombres_motorizado, personal_motorizado.apellidos_motorizado
order by personal_motorizado.cod_motorizado asc

---Segunda Consulta- Historico de dinero recaudado
select
entrega.cod_area as Codigo_Area,
area.nombre_area as Nombre_Area,
sum (factura.comision) as Comision,
extract (year from entrega.fecha_entrega) as Fecha
from factura
inner join entrega on factura.cod_entrega=entrega.cod_entrega
inner join area on entrega.cod_area=area.cod_area
group by extract (year from entrega.fecha_entrega),
entrega.cod_area, area.nombre_area, factura.comision 
order by entrega.cod_area asc



---Tercer Consulta- Historico de unidades dañadas
select
extract (year from revision.fecha_revision) as Fecha,
personal_motorizado.nombres_motorizado as Nombres,
personal_motorizado.apellidos_motorizado as Apellidos,
moto.cod_moto as Codigo_moto,
count (moto.cod_moto) as Veces_Dañadas
from revision 
inner join personal_motorizado on revision.cod_motorizado=personal_motorizado.cod_motorizado
inner join moto on revision.cod_moto=moto.cod_moto
where revision.estado_moto = 'Dañada'
group by extract (year from revision.fecha_revision), personal_motorizado.nombres_motorizado,
personal_motorizado.apellidos_motorizado, moto.cod_moto
order by moto.cod_moto asc


---Cuarta Cosnulta- Historico de solicitudes de clientes
select
cliente.nombres_cliente as Nombres,
cliente.apellidos_cliente as Apellidos,
extract (year from solicitud.fecha_solicitud) as Fecha,
count (cliente.nombres_cliente) as Numero_Solicitudes
from solicitud
inner join cliente on solicitud.cod_cliente=cliente.cod_cliente
group by extract (year from solicitud.fecha_solicitud), cliente.nombres_cliente, cliente.apellidos_cliente